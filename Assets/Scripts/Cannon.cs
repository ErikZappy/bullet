﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Cannon : MonoBehaviour {

    public float speed = 1;
    public float bulletSpd = 5f;
    public float rad;
    public float tick;
    public BulletManager bulletManager;
    public Player player;
    public Text pointTracker;

    private Transform parent;
    private float timer = 0;
    private float time = 0f;

	// Use this for initialization
	void Start () {
        parent = transform.parent;
    }

    // Update is called once per frame
    void Update () {
        time += Time.deltaTime;

        orbit(parent, time);
        timer += Time.deltaTime;
        if (timer > tick)
        {
            shootBullet();
            timer = 0;
        }
    }

    public void reset()
    {
        timer = 0;
    }

    void shootBullet()
    {
        Bullet b = bulletManager.getAvailableBullet();

        // setting bullet location
        b.transform.position = this.transform.position;

        // calculating bullet velocity and direction
        float x = this.transform.position.x-parent.position.x;
        float y = this.transform.position.y-parent.position.y;
        Vector2 bulletVel = new Vector2(x, y);
        bulletVel.Normalize();
        bulletVel = bulletVel * bulletSpd;
        b.GetComponent<Rigidbody2D>().velocity = bulletVel;
        b.speed = bulletSpd;

        // incrementing
        player.points++;
        pointTracker.text = "Points: " + player.points;

    }

    void orbit(Transform parent, float time){
        // position of the object
        Vector2 newPosition = new Vector2(0, 0);
        float newX = parent.position.x + rad * Mathf.Cos(speed * time);
        float newY = parent.position.y + rad * Mathf.Sin(speed * time);
        newPosition.x = newX;
        newPosition.y = newY;

        // rotation of the object
        float deltaX = transform.position.x - newX;
        float deltaY = transform.position.y - newY;
        float deltaPos = (deltaX * deltaX) + (deltaY * deltaY);
        float lawOfCosines = 1 - (deltaPos / (2 * rad * rad));
        float deltaTheta = Mathf.Acos(lawOfCosines);
        deltaTheta = deltaTheta / (2 * Mathf.PI) * 360;
        Vector3 eularRot = transform.rotation.eulerAngles;
        eularRot.z = eularRot.z + deltaTheta;
        

        // settings the position    
        transform.position = newPosition;
        //setting the new rotation
        transform.rotation = Quaternion.Euler(eularRot);
    }
}
