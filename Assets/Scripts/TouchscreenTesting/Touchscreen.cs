﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Touchscreen : MonoBehaviour {

    public Text t1;
    public Text t2;
    public Text t3;
    public Text t4;
    public GameObject cube;

    protected Vector2 lastPos;
    protected Vector2 pos;
    protected Vector2 vel;
    protected Vector2 velRigid;

	// Use this for initialization
	void Start () {
        pos = new Vector2(0, 0);
	}
	
	// Update is called once per frame
	void Update () {
        printToText(t1, "Cube Pos: \n" + cube.transform.position);

        if(Input.touches.Length != 0)
        {
            Touch tch = Input.touches[0];
            if (tch.phase == TouchPhase.Began)
            {
                lastPos = Camera.main.ScreenToWorldPoint(tch.position);
            }
            else if (tch.phase == TouchPhase.Moved)
            {
                pos = Camera.main.ScreenToWorldPoint(tch.position);
                printToText(t2, "Pos:\n" + pos.ToString() + 
                                "\nLastPos:\n" + lastPos.ToString());

                vel = pos - lastPos;
                if(vel.magnitude > 0.5f)
                {
                    vel = vel.normalized * 0.5f;
                }
                printToText(t3, "Delta Pos:\n" + vel.ToString());
                cube.transform.position = cube.transform.position + new Vector3(vel.x, vel.y);
                lastPos = pos;
            }
        }
	}

    void FixedUpdate()
    {
        velRigid = pos - lastPos;
        printToText(t4, "Vel Rigid:\n" + vel.ToString());
    }

    void printToText(Text t, string str)
    {
        t.text = str;
    }
}
