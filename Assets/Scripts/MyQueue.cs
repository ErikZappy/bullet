﻿using System;
using UnityEngine;
using System.Collections;

public class MyQueue<T>
{
    private Node<T> head;
    private Node<T> tail;
    private int size;

	public MyQueue()
	{
        head = null;
        tail = null;
        size = 0;
	}

    public void enqueue(T x)
    {
        Node<T> newNode = new Node<T>(x);
        if (size == 0)
            head = newNode;
        else
            tail.setNext(newNode);
        tail = newNode;
        size++;
    }

    public T dequeue()
    {
        if (head == null)
            return default(T);
        Node<T> ret = head;
        head = head.getNext();
        size--;
        return ret.data;
    }

    public void reset()
    {
        head = null;
        tail = null;
        size = 0;
    }

    override public string ToString()
    {
        Node<T> curr = head;
        string str = "";

        // always need to make sure that this exits the loop woops
        while(curr != null)
        {
            if (curr.data == null)
                str += "Null\n";
            else
                str += curr.data.ToString() + "\n";
            curr = curr.getNext();
        }

        return str;
    }

    // keeps a record of an open bullet to return
    class Node<T>
    {
        public T data;
        // points to the next unused bullet node in the array
        // is equal to -1 when it is the tail
        Node<T> next;
        public Node()
        {
            next = null;
            data = default(T);
        }

        public Node(T data)
        {
            this.next = null;
            this.data = data;
        }

        public Node<T> getNext()
        {
            return next;
        }
        
        public void setNext(Node<T> next)
        {
            this.next = next;
        }
    }
}
