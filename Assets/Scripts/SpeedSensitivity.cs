﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SpeedSensitivity : MonoBehaviour {

    public float sensitivity = 5;
    public float speed = 0.5f;

    public Slider sensitivitySlider;
    public Slider speedSlider;

    public Text sensetivityText;
    public Text speedText;

    private bool menu = true;

	// Use this for initialization
	void Start () {
        if(menu)
            DontDestroyOnLoad(this);
	}
	
	// Update is called once per frame
	void Update () {
        if(menu)
        sensitivity = sensitivitySlider.value;
        speed = speedSlider.value;

        sensetivityText.text = sensitivity.ToString();
        speedText.text = speed.ToString();
	}

    public void loadLevel(int level)
    {
        menu = false;
        SceneManager.LoadScene(level, LoadSceneMode.Single);
    }
}
