﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Movement : MonoBehaviour {

    public float sensitivity = 6f;
    public float speedCap = 0.5f;

    private SpeedSensitivity values;
    private Vector2 lastPos;
    private Vector2 pos;
    private Vector2 vel = new Vector2(0, 0);

	// Use this for initialization
	void Start () {
        try
        {
            values = GameObject.Find("SpeedCapAndSensitivity").GetComponent<SpeedSensitivity>();
            sensitivity = values.sensitivity;
            speedCap = values.speed;
        }
        catch{}
        lastPos = transform.position;
    }

    // Update is called once per frame
    // lets try touch screen stuff!
    void Update() {
        if (Input.touches.Length != 0)
        {
            Touch tch = Input.touches[0];
            if (tch.phase == TouchPhase.Began)
            {
                lastPos = Camera.main.ScreenToWorldPoint(tch.position);
            }
            else if (tch.phase == TouchPhase.Moved)
            {
                pos = Camera.main.ScreenToWorldPoint(tch.position);

                vel = pos - lastPos;
                if (vel.magnitude > speedCap)
                {
                    vel = vel.normalized * speedCap;
                }
                transform.position = transform.position + new Vector3(vel.x, vel.y);
                lastPos = pos;
            }
        }
    }

    public void reset()
    {
        lastPos = transform.position;
    }
}
