﻿using UnityEngine;
using System.Collections;

public class BulletManager : MonoBehaviour {

    public GameObject bull;
    MyQueue<Bullet> queue = new MyQueue<Bullet>();
	
    // Use this for initialization
	void Start (){
        resetAll();
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    public void resetAll()
    {
        queue.reset();
        foreach(Transform child in transform)
        {
            child.GetComponent<Bullet>().resetHits();
            queue.enqueue(child.GetComponent<Bullet>());
            child.gameObject.SetActive(false);
        }
    }

    public void destroyBullet(Bullet bull)
    {
        bull.gameObject.SetActive(false);
        queue.enqueue(bull);
    }

    public Bullet getAvailableBullet()
    {
        Bullet bullet = queue.dequeue();
        GameObject b;
        if (bullet == null)
            b = Instantiate(bull, transform.position, Quaternion.identity) as GameObject;
        else
            b = bullet.transform.gameObject;

        b.GetComponent<Bullet>().bulletManager = this;
        b.transform.SetParent(this.transform);
        b.SetActive(true);
        return b.GetComponent<Bullet>();
    }
}
