﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour {

    public int points = 0;
    public BulletManager bManager;
    public Text gameOverScreen;
    public Text pointerTracker;
    public Button resetButton;
    public Button resetLevelButton;

	// Use this for initialization
	void Start () {
        gameOverScreen.gameObject.SetActive(false);
        resetButton.gameObject.SetActive(false);
        resetLevelButton.gameObject.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    // maybe just fake dying and then reset all the variables
    // as to not reload the level but idk
    public void dead()
    {
        gameObject.SetActive(false);
        gameOverScreen.gameObject.SetActive(true);
        resetButton.gameObject.SetActive(true);
        resetLevelButton.gameObject.SetActive(true);
        gameOverScreen.text += "\n Total Points: " + points;
        pointerTracker.gameObject.SetActive(false);
    }

    public void reset()
    {
        points = 0;
        bManager.resetAll();
        pointerTracker.text = "Points: 0";
        pointerTracker.gameObject.SetActive(true);
        gameOverScreen.text = "Game Over";
        gameOverScreen.gameObject.SetActive(false);
        gameObject.SetActive(true);
        gameObject.transform.position = new Vector2(0, 0);
        resetButton.gameObject.SetActive(false);
        resetLevelButton.gameObject.SetActive(false);
        GetComponentInChildren<Cannon>().reset();
        gameObject.GetComponent<Movement>().reset();
    }

    public void resetLevel()
    {
        GameObject go = GameObject.Find("SpeedCapAndSensitivity");
        DestroyObject(go);
        SceneManager.LoadScene(0, LoadSceneMode.Single);
    }
}
