﻿using UnityEngine;
using System.Collections;

public class Bullet : MonoBehaviour {

    public BulletManager bulletManager;
    public float speed = 2;
    readonly public int hitsAvailable = 10;
    private int hits = 0;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void resetHits()
    {
        hits = 0;
    }

    void OnCollisionEnter2D(Collision2D coll)
    {
        ContactPoint2D contact = coll.contacts[0];
        Vector2 reflectedVec = getReflection(contact.normal, GetComponent<Rigidbody2D>().velocity);
        GetComponent<Rigidbody2D>().velocity = reflectedVec;

        // 10 is the player layer
        if(coll.collider.gameObject.layer == 10)
        {
            coll.collider.gameObject.GetComponent<Player>().dead();
        }


        // 8 is the layer number of the walls
        if (coll.collider.gameObject.layer == 8)
            hits++;

        if (hits > hitsAvailable)
        {
            hits = 0;
            bulletManager.destroyBullet(this);
        }
    }

    Vector2 getReflection(Vector2 normal, Vector2 input)
    {
        Vector2 reflectedVec;
        float scalar = Vector2.Dot(normal, input) * 2;
        reflectedVec = input - scalar * normal;
        return reflectedVec;
    }

    override public string ToString()
    {
        string str = "";
        str += "I am " + gameObject.name;
        return str;
    }
}
